package com.findgopal.marsplay

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.CameraView
import com.otaliastudios.cameraview.Facing
import com.otaliastudios.cameraview.Flash
import kotlinx.android.synthetic.main.activity_camera.*

class CameraActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var camera: CameraView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED)
        setContentView(R.layout.activity_camera)

        camera = findViewById(R.id.cameraView)
        camera.setLifecycleOwner(this)
        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(jpeg: ByteArray) {
                onPicture(jpeg)
            }
        })

        flash.setOnClickListener(this)
        capturePhoto.setOnClickListener(this)
        toggleCamera.setOnClickListener(this)

    }

    private fun message(content: String, important: Boolean) {
        val length = if (important) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
        Toast.makeText(this, content, length).show()
    }

    private fun onPicture(jpeg: ByteArray) {
        PreviewActivity.setImage(jpeg)
        startActivity(Intent(this@CameraActivity, PreviewActivity::class.java))
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.flash -> flash()
            R.id.capturePhoto -> capturePhoto()
            R.id.toggleCamera -> toggleCamera()
        }
    }

    private fun flash() {
        if (camera.flash == Flash.OFF) {
            camera.flash = Flash.AUTO
            flash.setImageResource(R.drawable.ic_auto_flash)
            return
        }
        if (camera.flash == Flash.AUTO) {
            camera.flash = Flash.ON
            flash.setImageResource(R.drawable.ic_flash)
            return
        }
        if (camera.flash == Flash.ON) {
            camera.flash = Flash.OFF
            flash.setImageResource(R.drawable.ic_flash_off)
            return
        }
    }

    private fun capturePhoto() {
        message("Capturing picture...", false)
        camera.capturePicture()
    }

    private fun toggleCamera() {
        when (camera.toggleFacing()) {
            Facing.BACK -> {
                message("Switched to back camera!", false)
                flash.visibility = View.VISIBLE
            }
            Facing.FRONT -> {
                message("Switched to front camera!", false)
                flash.visibility = View.INVISIBLE
            }
        }
    }

}