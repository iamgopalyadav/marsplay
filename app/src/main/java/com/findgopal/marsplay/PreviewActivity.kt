package com.findgopal.marsplay

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.otaliastudios.cameraview.CameraUtils
import com.takusemba.cropme.CropView
import com.takusemba.cropme.OnCropListener
import kotlinx.android.synthetic.main.activity_preview.*
import java.lang.ref.WeakReference


class PreviewActivity : AppCompatActivity(), OnCropListener {

    companion object {
        private lateinit var image: WeakReference<ByteArray>
        fun setImage(im: ByteArray?) {
            if (im != null)
                image = WeakReference(im)
        }
    }

    private lateinit var imageView: CropView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)
        imageView = findViewById(R.id.crop_view)
        progressBar = findViewById(R.id.progress)
        val b = image.get()
        if (b == null) {
            finish()
            return
        }

        CameraUtils.decodeBitmap(b, 1000, 1000) { bitmap ->
            imageView.setBitmap(bitmap)
            UploadActivity.setImage(bitmap)
            Log.d("CameraUtils", bitmap.width.toString())
            Log.d("CameraUtils", bitmap.height.toString())
        }

        crop_button.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            imageView.crop(this)
        }

        no_crop_button.setOnClickListener {
            startActivity(Intent(this@PreviewActivity, UploadActivity::class.java))
            finish()
        }

        cross.setOnClickListener {
            finish()
        }
    }

    override fun onSuccess(bitmap: Bitmap?) {
        progressBar.visibility = View.GONE
        saveBitmapAndStartActivity(bitmap!!)
    }

    override fun onFailure() {
        progressBar.visibility = View.GONE
    }

    private fun saveBitmapAndStartActivity(bitmap: Bitmap) {
        progressBar.visibility = View.VISIBLE
        imageView.isEnabled = false
        Thread(Runnable {
            runOnUiThread {
                UploadActivity.setImage(bitmap)
                progressBar.visibility = View.GONE
                imageView.isEnabled = true
                startActivity(Intent(this@PreviewActivity, UploadActivity::class.java))
                finish()
            }
        }).start()
    }

}