package com.findgopal.marsplay

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.images_list_item.view.*

class ImagesAdapter(val imageList: ArrayList<String>, private val context: Context) : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.get().load(imageList[holder.adapterPosition]).into(holder.image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val context = context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.images_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return imageList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val image: ImageView = view.imageView

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val photoUrl = imageList[position]
                val intent = Intent(context, PhotoActivity::class.java).apply {
                    putExtra("PHOTO_URL", photoUrl)
                }
                startActivity(context, intent, null)
            }
        }

    }

}
