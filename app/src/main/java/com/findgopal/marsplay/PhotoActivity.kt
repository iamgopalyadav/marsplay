package com.findgopal.marsplay

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import com.squareup.picasso.Picasso

class PhotoActivity : AppCompatActivity() {

    private lateinit var imageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)
        imageView = findViewById(R.id.image)

        val photoUrl = intent.getStringExtra("PHOTO_URL")
        Log.d("PhotoActivity",photoUrl)
        Picasso.get()
                .load(photoUrl)
                .into(imageView)
    }
}
