package com.findgopal.marsplay

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import kotlinx.android.synthetic.main.activity_upload.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.ref.WeakReference
import java.util.*


class UploadActivity : AppCompatActivity() {

    companion object {
        private lateinit var image: WeakReference<Bitmap>
        fun setImage(im: Bitmap?) {
            if (im != null)
                image = WeakReference(im)
        }
    }

    private lateinit var s3Client: AmazonS3Client
    private lateinit var transferUtility: TransferUtility
    private var bucket = "marsplay-bucket/photos"

    private lateinit var uploadToS3: File
    private lateinit var icon: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)

        val credentialsProvider = CognitoCachingCredentialsProvider(
                applicationContext,
                "us-east-1:82476c77-c46f-4cd9-8c79-f02902149a11",
                Regions.US_EAST_1
        )

        createAmazonS3Client(credentialsProvider)

        cross.setOnClickListener {
            finish()
        }

        val b = UploadActivity.image.get()
        if (b == null) {
            finish()
            return
        } else {
            photoView.setImageBitmap(b)
            val currentTime = Calendar.getInstance().time
            val fileName = "marsplay_" + currentTime.time.toString() + ".jpg"
            uploadToS3 = File(this.cacheDir, fileName)
            uploadToS3.createNewFile()

            val bos = ByteArrayOutputStream()
            b.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
            val bmpData = bos.toByteArray()

            val fos = FileOutputStream(uploadToS3)
            fos.write(bmpData)
            fos.flush()
            fos.close()
        }

        upload_button.setOnClickListener {
            uploadFileToS3(it)
        }

        icon = getBitmapFromVectorDrawable(this, R.drawable.ic_tick)

    }

    private fun uploadFileToS3(view: View) {
        val currentTime = Calendar.getInstance().time
        val fileName = "marsplay_" + currentTime.time.toString() + ".jpg"
        val transferObserver = transferUtility.upload(
                bucket,
                fileName,
                uploadToS3
        )

        transferObserverListener(transferObserver)
    }

    private fun createAmazonS3Client(credentialsProvider: CognitoCachingCredentialsProvider) {

        s3Client = AmazonS3Client(credentialsProvider)
        s3Client.setRegion(Region.getRegion(Regions.US_EAST_1))
        setTransferUtility()
    }

    private fun setTransferUtility() {

        transferUtility = TransferUtility(s3Client,
                applicationContext)
    }

    private fun transferObserverListener(transferObserver: TransferObserver) {

        transferObserver.setTransferListener(object : TransferListener {

            override fun onStateChanged(id: Int, state: TransferState) {
                Log.d("UploadActivity", "State Change$state")
                upload_button.startAnimation()
                if (state == TransferState.COMPLETED) {
                    upload_button.doneLoadingAnimation(R.color.green, icon)
                    Handler().postDelayed({
                        startActivity(Intent(this@UploadActivity, GalleryActivity::class.java))
                        finish()
                    }, 1500)
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val percentage = (bytesCurrent / bytesTotal * 100).toInt()
                Log.d("UploadActivity", "Progress in %$percentage")
            }

            override fun onError(id: Int, ex: Exception) {
                Log.e("error", "error")
            }

        })
    }

    private fun getBitmapFromVectorDrawable(context: Context, drawableId: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableId)

        val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth,
                drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }


    override fun onDestroy() {
        super.onDestroy()
        upload_button.dispose()
    }
}
