package com.findgopal.marsplay

import android.content.res.Configuration
import android.os.Bundle
import android.os.Looper
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import kotlinx.android.synthetic.main.activity_gallery.*


class GalleryActivity : AppCompatActivity() {

    private lateinit var s3Client: AmazonS3Client
    private lateinit var transferUtility: TransferUtility
    private var bucket = "marsplay-bucket"
    private lateinit var imageList: ArrayList<String>
    private lateinit var adapter: ImagesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)

        val credentialsProvider = CognitoCachingCredentialsProvider(
                applicationContext,
                "us-east-1:82476c77-c46f-4cd9-8c79-f02902149a11",
                Regions.US_EAST_1
        )

        imageList = ArrayList()

        createAmazonS3Client(credentialsProvider)

        fetchFilesFromS3()

        rv_images.setHasFixedSize(true)

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            rv_images.layoutManager = GridLayoutManager(this, 3)
        } else rv_images.layoutManager = GridLayoutManager(this, 2)

        adapter = ImagesAdapter(imageList, this)
        rv_images.adapter = adapter

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            rv_images.layoutManager = GridLayoutManager(this, 3)
        } else rv_images.layoutManager = GridLayoutManager(this, 2)
        super.onConfigurationChanged(newConfig)
        adapter.notifyDataSetChanged()
    }

    private fun createAmazonS3Client(credentialsProvider: CognitoCachingCredentialsProvider) {

        s3Client = AmazonS3Client(credentialsProvider)
        s3Client.setRegion(Region.getRegion(Regions.US_EAST_1))
        setTransferUtility()
    }

    private fun setTransferUtility() {

        transferUtility = TransferUtility(s3Client,
                applicationContext)
    }

    private lateinit var listing: List<String>

    private fun fetchFilesFromS3() {

        val thread = Thread(Runnable {
            try {
                Looper.prepare()
                listing = getObjectNamesForBucket(bucket, s3Client)

                for (i in 0 until listing.size) {
                    imageList.add("https://s3.amazonaws.com/$bucket/" + listing[i])
                    Log.d("GalleryActivity", imageList[i])
                }
                runOnUiThread {
                    adapter.notifyDataSetChanged()
                }
                Looper.loop()
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("tag", "Exception found while listing $e")
            }
        })
        thread.start()
    }

    private fun getObjectNamesForBucket(bucket: String, s3Client: AmazonS3): List<String> {
        var objects = s3Client.listObjects(bucket)
        val objectNames = ArrayList<String>(objects.objectSummaries.size)
        var iterator = objects.objectSummaries.iterator()
        while (iterator.hasNext()) {
            objectNames.add(iterator.next().key)
        }
        while (objects.isTruncated) {
            objects = s3Client.listNextBatchOfObjects(objects)
            iterator = objects.objectSummaries.iterator()
            while (iterator.hasNext()) {
                objectNames.add(iterator.next().key)
            }
        }
        return objectNames
    }

}
